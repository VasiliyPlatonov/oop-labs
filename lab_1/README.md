
####For generate javadoc use:

`_javadoc -d ./oop-labs/lab_1_javadoc -private oop-labs/lab_1/src/main/java/Main.java_`


 **there:**
 
  -**_d_** <br>
  `<directory> Destination directory for output files`
                  
  **_-private_**<br>
                  `Show all types and members. For named modules,`
                  `show all packages and all module details.`