import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Laboratory 1
 *
 * @author Vasiliy Platonov
 * @version 1.0
 */
public class Main {
    private static int[] array = new int[10];

    public static void main(String[] args) {
        int[] randArr = getRandomlyFilledArray(array);
        System.out.println("The array randomly filled: " + Arrays.toString(randArr));

        int[] ordArr = getOrderedArray(randArr);
        System.out.println("The ordered array: " + Arrays.toString(ordArr));
    }

    /**
     * Returns an array filled with random integer values
     *
     * @param array array of integers to be filled with random values
     * @return array of integer with random values
     * @throws NullPointerException     If the {@code array} is {@code null}
     * @throws IllegalArgumentException If the {@code array} has length less than 1
     */
    private static int[] getRandomlyFilledArray(int[] array) {
        if (array == null) {
            throw new NullPointerException("The array must be not null");
        }
        if (array.length < 1) {
            throw new IllegalArgumentException("The array must has length more than 1");
        }

        for (int i = 0; i < array.length; i++) {
            array[i] = ThreadLocalRandom.current().nextInt(100);
        }
        return array;
    }

    /**
     * Returns an ordered array
     *
     * @param array array of integers
     * @return array of integer with ordered values
     * @throws NullPointerException     If the {@code array} is {@code null}
     * @throws IllegalArgumentException If the {@code array} has length less than 1
     */
    private static int[] getOrderedArray(int[] array) {
        Objects.requireNonNull(array, "The array must be not null");

        if (array.length < 1) {
            throw new IllegalArgumentException("The array must has length more than 1");
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] <= array[j]) {
                    int x = array[i];
                    array[i] = array[j];
                    array[j] = x;
                }
            }
        }
        return array;
    }
}