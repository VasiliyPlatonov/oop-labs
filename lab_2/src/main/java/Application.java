import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Экранная форма предназначена для отображения списка фильмов,
 * она может менять свой размер на экране. Форма реализовывает следующие
 * функции: загрузку списка фильмов из файла, сохранение списка фильмов в файле,
 * добавление фильма в список, редактирование данных о фильме
 * удаление фильма из списка, поиск фильма по названию, режиссеру
 * вывод информации о приложении.
 *
 * @author Vasiliy Platonov
 * @version 1.0
 */
public class Application {
    /**
     * Окно для приложения
     */
    private JFrame bookList;

    /**
     * Модель таблицы
     */
    private DefaultTableModel model;

    /**
     * Кнопка 'добавить фильм'
     */
    private JButton add;

    /**
     * Кнопка 'удалить фильм'
     */
    private JButton delete;

    /**
     * Кнопка 'редактировать фильм'
     */
    private JButton edit;

    /**
     * Кнопка 'сохранить фильм'
     */
    private JButton save;

    /**
     * Кнопка 'вывести информацию'
     */
    private JButton info;

    /**
     * Панель инстументов
     */
    private JToolBar toolBar;

    /**
     * Скролл
     */
    private JScrollPane scroll;

    /**
     *
     */
    private JTable movie;

    /**
     *
     */
    private JComboBox director;

    /**
     *
     */
    private JTextField movieName;

    /**
     *
     */
    private JButton filter;


    /**
     * Показывает список фильмов
     */
    public void show() {
        // Создание окна
        bookList = new JFrame("Список фильмов");
        bookList.setSize(500, 300);
        bookList.setLocation(100, 100);
        bookList.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Создание кнопок и прикрепление иконок
        add = new JButton(new ImageIcon(this.getClass().getResource("img/add.png")));
        add.setToolTipText("Добавить фильм в список");

        delete = new JButton(new ImageIcon(this.getClass().getResource("img/remove.png")));
        delete.setToolTipText("Удалить фильм из списка");

        edit = new JButton(new ImageIcon(this.getClass().getResource("img/notepad.png")));
        edit.setToolTipText("Редактировать фильм");

        save = new JButton(new ImageIcon(this.getClass().getResource("img/save.png")));
        save.setToolTipText("Сохранить список фильмов");

        info = new JButton(new ImageIcon(this.getClass().getResource("img/info.png")));
        info.setToolTipText("Информация");


        // Добавление кнопок на панель инструментов
        toolBar = new JToolBar("Панель инструментов");
        toolBar.add(add);
        toolBar.add(delete);
        toolBar.add(edit);
        toolBar.add(save);
        toolBar.add(info);
        // Размещение панели инструментов
        bookList.setLayout(new BorderLayout());
        bookList.add(toolBar, BorderLayout.NORTH);


        // Создание таблицы с данными
        String[] columns = {"Название", "Режиссер", "Год выпуска"};
        String[][] data = {
                {"Оно 2", "Андрес Мусскетти", "2019"},
                {"Мстители", "Джосс Уидон", "2012"},
                {"Служебный роман", "Эльдар Рязанов", "1977"},
                {"Любовь и голуби", "Владимир Меньшов", "1984"}};
        model = new DefaultTableModel(data, columns);
        movie = new JTable(model);
        scroll = new JScrollPane(movie);
// Размещение таблицы с данными
        bookList.add(scroll, BorderLayout.CENTER);

// Подготовка компонентов поиска
        director = new JComboBox(
                new String[]{"Режиссер",
                        "Андрес Мусскетти",
                        "Джосс Уидон",
                        "Эльдар Рязанов",
                        "Владимир Меньшов"});
        movieName = new JTextField("Название фильма");
        filter = new JButton("Поиск");
// Добавление компонентов на панель
        JPanel filterPanel = new JPanel();
        filterPanel.add(director);
        filterPanel.add(movieName);
        filterPanel.add(filter);
// Размещение панели поиска внизу окна
        bookList.add(filterPanel, BorderLayout.SOUTH);
// Визуализация экранной формы
        bookList.setVisible(true);
    }

    public static void main(String[] args) {
// Создание и отображение экранной формы
        new Application().show();
    }


}
