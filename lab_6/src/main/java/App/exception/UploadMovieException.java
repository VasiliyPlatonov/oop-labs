package App.exception;

public class UploadMovieException extends Exception {
    public UploadMovieException(String message) {
        super(message);
    }

    public UploadMovieException(String message, Throwable cause) {
        super(message, cause);
    }
}
