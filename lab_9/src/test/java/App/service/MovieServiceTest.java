package App.service;

import App.exception.ValidationException;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class MovieServiceTest {

    @Test
    public void checkYear() throws Exception {
        List<String> badYears = Arrays.asList("1700", "30001", "201s9", "");
        List<String> goodYears = Arrays.asList("1900", "2019", "1942");

        // Negative tests -------
        // для каждого не подходящего значени года кидается исключени "ValidationException"
        for (String badYear : badYears) {
            assertThatThrownBy(() -> MovieService.checkYear(badYear))
                    .isInstanceOf(ValidationException.class);
        }

        // Проверка правильности сообщения
        assertThatThrownBy(() -> MovieService.checkYear(badYears.get(0)))
                .hasMessageContaining("Год выхода фильма должен быть больше 1800 и меньше 2039 !");

        // Positive tests
        // каждое  подходящее значение года проходит
        for (String goodYear : goodYears) {
            MovieService.checkYear(goodYear);
        }
    }

    @Test
    public void checkMovieName() throws Exception {
        List<String> badNames = Collections.singletonList("");
        List<String> goodNames = Arrays.asList("Красотка", "От заката до рассвета", "Москва слезам не верит");

        // Negative tests -------
        // для каждого не подходящего значения  названия фильма кидается исключени "ValidationException"
        for (String name : badNames) {
            assertThatThrownBy(() -> MovieService.checkMovieName(name))
                    .isInstanceOf(ValidationException.class);
        }

        // Positive tests
        // каждое  подходящее значение года проходит
        for (String name : goodNames) {
            MovieService.checkMovieName(name);
        }
    }
}