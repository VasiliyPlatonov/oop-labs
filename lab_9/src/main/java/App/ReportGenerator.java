package App;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRXmlUtils;
import org.w3c.dom.Document;

import java.io.File;
import java.util.Date;
import java.util.HashMap;


public class ReportGenerator {
    final static String sep = File.separator;
    /*
     * http://jasperreports.sourceforge.net/sample.reference/xmldatasource/
     * https://medium.com/@seymorethrottle/jasper-reports-adding-custom-fonts-589b55a52e7c
     */
    public static void generate(String dataSource, String template, String resultPath) {
//        File file = new File("resources" + sep + "report" + sep + fileName);
//        String resultPath = file.getAbsolutePath();

        System.out.println("--------- Начинается создание отчета ---------");
        System.out.println(
                "Путь к источнику данных(в формате xml): " + dataSource + "\n" +
                        "Путь к результирующему файлу отчета: " + resultPath
        );
        long start = System.currentTimeMillis();
        HashMap<String, Object> params = new HashMap<>();
        params.put("DATE", new Date());



        try {
            // Указание источника XML-данных
            Document document = JRXmlUtils.parse(JRLoader.getLocationInputStream(dataSource));
            params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, document);
            // Создание отчета на базе dataSource
            JasperReport jasperReport = JasperCompileManager.compileReport(JRLoader.getLocationInputStream(template));
            // Заполнение отчета данными
            JasperPrint print = JasperFillManager.fillReport(jasperReport, params);
            if (resultPath.toLowerCase().endsWith("pdf")) {
                JasperExportManager.exportReportToPdfFile(print, resultPath);
            } else {
                JasperExportManager.exportReportToHtmlFile(print, resultPath);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        System.out.println("Отчет создан.\nЗатрачено времени: " + (System.currentTimeMillis() - start) + " миллисекунд");
    }
//
//    public static void main(String[] args) {
//        String dataSource = "testData.xml";
//        String template = "report.jrxml";
//        String fileName = "result.html";
//
//        generate(dataSource, template, fileName);
//    }

}