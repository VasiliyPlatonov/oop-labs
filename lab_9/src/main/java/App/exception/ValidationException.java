package App.exception;

/**
 * <p>
 * The {@code ValidationException} is thrown when
 * user tries to save or edit illegal property of movie.
 * <p/>
 *
 * @author Vasiliy Platonov
 */
public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
