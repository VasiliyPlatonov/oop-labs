package App.exception;

public class DownloadMoviesException extends Exception {
    public DownloadMoviesException(String message) {
        super(message);
    }
}
