package App.view;

import App.controller.MovieListController;
import App.controller.MovieListFileController;
import App.exception.DownloadMoviesException;
import App.exception.EmptyMovieListException;
import App.exception.UploadMovieException;
import App.exception.ValidationException;
import App.model.Movie;
import App.model.MovieTableModel;
import App.view.util.FileFilterExt;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static javax.swing.WindowConstants.*;

public class MovieListFrame {
    /**
     * Окно для приложения
     */
    private JFrame frame;

    /**
     * Модель таблицы
     */
    private DefaultTableModel model;

    /**
     * Кнопка 'добавить фильм'
     */
    private JButton add;

    /**
     * Кнопка 'удалить фильм'
     */
    private JButton delete;

    /**
     * Кнопка 'редактировать фильм'
     */
    private JButton edit;

    /**
     * Кнопка 'сохранить список фильмов в файл'
     */
    private JButton upload;

    /**
     * Кнопка 'загрузить фильмовов из файл'
     */
    private JButton download;

    /**
     * Кнопка 'создать отчет'
     */
    private JButton report;


    /**
     * Кнопка 'вывести информацию'
     */
    private JButton info;

    /**
     * Панель инстументов
     */
    private JToolBar toolBar;

    /**
     * Скролл
     */
    private JScrollPane scroll;

    /**
     *
     */
    private JTable movieTable;

    /**
     *
     */
    private JComboBox director;

    /**
     *
     */
    private JTextField movieName;

    /**
     *
     */
    private JButton filter;


    private final String[][] FILE_FILTERS = {
            {"xml", "Файлы XML (*.xml)"},
            {"txt", "Файлы TXT (*.txt)"}};

    private final String[][] REPORT_FILTERS = {
            {"pdf", "Файлы PDF (*.pdf)"},
            {"html", "Файлы HTML (*.html)"}};


    private MovieListController controller;
    private List<Movie> movies;

    public MovieListFrame() {
        movies = new ArrayList<>();
        controller = new MovieListFileController(movies);
    }

    /**
     * Показывает список фильмов
     */
    public void show() {
        // Создание окна
        frame = new JFrame("Список фильмов");
        frame.setSize(500, 600);
        frame.setLocation(100, 100);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // ничего не делаем, т.к. будет повешен слушатель

        // Создание кнопок и прикрепление иконок
        ImageIcon addIcon = new ImageIcon(this.getClass().getResource("../../img/add.png"));
        add = new JButton(addIcon);
        add.setToolTipText("Добавить фильм в список");

        ImageIcon deleteIcon = new ImageIcon(this.getClass().getResource("../../img/remove.png"));
        delete = new JButton(deleteIcon);
        delete.setToolTipText("Удалить фильм из списка");

        ImageIcon editIcon = new ImageIcon(this.getClass().getResource("../../img/pen.png"));
        edit = new JButton(editIcon);
        edit.setToolTipText("Редактировать фильм");

        ImageIcon uploadIcon = new ImageIcon(this.getClass().getResource("../../img/upload.png"));
        upload = new JButton(uploadIcon);
        upload.setToolTipText("Сохранить список фильмов в файл");

        ImageIcon downloadIcon = new ImageIcon(this.getClass().getResource("../../img/download.png"));
        download = new JButton(downloadIcon);
        download.setToolTipText("Загрузить список фильмов из файла");

        ImageIcon reportIcon = new ImageIcon(this.getClass().getResource("../../img/notepad.png"));
        report = new JButton(reportIcon);
        report.setToolTipText("Создать отчет");


        ImageIcon infoIcon = new ImageIcon(this.getClass().getResource("../../img/info.png"));
        info = new JButton(infoIcon);
        info.setToolTipText("Информация");


        // Добавление кнопок на панель инструментов
        toolBar = new JToolBar("Панель инструментов");
        toolBar.add(add);
        toolBar.add(delete);
        toolBar.add(edit);
        toolBar.add(upload);
        toolBar.add(download);
        toolBar.add(report);
        toolBar.add(info);

        // Размещение панели инструментов
        frame.setLayout(new BorderLayout());
        frame.add(toolBar, BorderLayout.NORTH);

        // Создание и размещение таблицы с данными
        MovieTableModel tableModel = new MovieTableModel(movies, frame);
        movieTable = new JTable(tableModel);
        scroll = new JScrollPane(movieTable);
        frame.add(scroll, BorderLayout.CENTER);

        // Подготовка компонентов поиска
//        director = new JComboBox(
//                new String[]{"Режиссер",
//                        "Андрес Мусскетти",
//                        "Джосс Уидон",
//                        "Эльдар Рязанов",
//                        "Владимир Меньшов"});
        movieName = new JTextField("Название фильма");
        filter = new JButton("Поиск");
        // Добавление компонентов на панель
        JPanel filterPanel = new JPanel();
//        filterPanel.add(director);
        filterPanel.add(movieName);
        filterPanel.add(filter);
        // Размещение панели поиска внизу окна
        frame.add(filterPanel, BorderLayout.SOUTH);
        // Визуализация экранной формы
        frame.setVisible(true);


        // ***************************
        // Слушатели *****************
        //****************************

        /*
         Слушатель события для кнопки info.

         Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
         при появлении этого события, с помощью класса JOptionPane выводит на экран
         диалоговое окно, информирующее пользователя.
         */
        info.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame,
                        "'Список фильмов'" +
                                "\nприложение для создания списков просмотренных фильмов");
            }
        });

        /*
         Слушатель для фрейма bookList.

         Данный слушатель отслеживает появление события windowClosing (нажатие на кнопку закрытия окна)
         и, при появлении этого события, с помощью класса JOptionPane выводит на экран
         диалоговое окно, запрашивающее подтверждение на выход.
        */
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Object[] options = {"Да", "Нет!"};
                int n = JOptionPane
                        .showOptionDialog(e.getWindow(), "Вы уверены, что хотите выйти?",
                                "Подтверждение", JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null, options,
                                options[0]);
                if (n == 0) {
                    e.getWindow().setVisible(false);
                    System.exit(0);
                }
            }
        });

        /*
         Слушатель события для кнопки upload.

         Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
         при появлении этого события, пытается записать на диск текущий список фильмов пользователя
        */
        upload.addActionListener(e -> {
            JFileChooser jfc =
                    new JFileChooser(FileSystemView.getFileSystemView().getParentDirectory(new File("./")));
            jfc.setDialogTitle("Выберите директорию где будет сохранен список фильмов");
            jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            jfc.setAcceptAllFileFilterUsed(false);

            // Определяем фильтры типов файлов
            for (int i = 0; i < FILE_FILTERS[0].length; i++) {
                FileFilterExt eff = new FileFilterExt(FILE_FILTERS[i][0],
                        FILE_FILTERS[i][1]);
                jfc.addChoosableFileFilter(eff);
            }

            int returnValue = jfc.showSaveDialog(frame);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                try {
                    controller.save(jfc.getSelectedFile());
                    JOptionPane.showMessageDialog(frame, "Список фильмов был записан на диск");
                } catch (EmptyMovieListException | UploadMovieException ex) {
                    JOptionPane.showMessageDialog(frame, ex.getMessage());
                }
            }
        });

       /*
        Слушатель события для кнопки download.

        Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
        при появлении этого события, пытается загузить список фильмов из файла
        */
        download.addActionListener(e -> {
            JFileChooser jfc =
                    new JFileChooser(FileSystemView.getFileSystemView().getParentDirectory(new File("./")));
            jfc.setDialogTitle("Выберите файл для загрузки списка фильмов(txt или xml)");
            jfc.setAcceptAllFileFilterUsed(false);
            // Определяем фильтры типов файлов
            for (int i = 0; i < FILE_FILTERS[0].length; i++) {
                FileFilterExt eff = new FileFilterExt(FILE_FILTERS[i][0],
                        FILE_FILTERS[i][1]);
                jfc.addChoosableFileFilter(eff);
            }

            int returnValue = jfc.showOpenDialog(frame);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                try {
                    controller.readFromFile(jfc.getSelectedFile());
                    tableModel.fireTableDataChanged();
                } catch (DownloadMoviesException ex) {
                    JOptionPane.showMessageDialog(frame, ex.getMessage());
                }
            }

        });


        /*
        Слушатель события для кнопки add.

        Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
        при появлении этого события, пытается добавить фильм в список фильмов
        */
        add.addActionListener(e -> {
            JPanel myPanel = new JPanel();
            JTextField name = new JTextField(10);
            JTextField director = new JTextField(10);
            JTextField year = new JTextField(10);
            myPanel.add(new JLabel("Название: "));
            myPanel.add(name);
            myPanel.add(new JLabel("Режиссер: "));
            myPanel.add(director);
            myPanel.add(new JLabel("Год: "));
            myPanel.add(year);

            int exit =
                    JOptionPane.showConfirmDialog(frame, myPanel,
                            "Введите данные фильма",
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            addIcon);

            if (exit == JOptionPane.YES_OPTION) {
                try {
                    controller.addMovie(
                            new Movie(
                                    name.getText(),
                                    director.getText(),
                                    year.getText()));
                } catch (ValidationException ex) {
                    JOptionPane.showMessageDialog(frame, ex.getMessage());
                }
                tableModel.fireTableDataChanged();
            }
        });

         /*
        Слушатель события для кнопки delete.

        Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
        при появлении этого события, пытается удалить фильм выбранный из списка фильмов
        */

        delete.addActionListener(e -> {
            if (movies.isEmpty()) {
                JOptionPane.showMessageDialog(frame, "Список фильмов пуст!");
                return;
            }
            if (movieTable.getSelectedRowCount() > 1) {
                int[] selectedRows = movieTable.getSelectedRows();

                for (int i = selectedRows.length - 1; i >= 0; i--) {
                    controller.removeMovie(movies.get(selectedRows[i]));
                }

            } else {
                controller.removeMovie(movies.get(movieTable.getSelectedRow()));
            }

            tableModel.fireTableDataChanged();
        });


         /*
         Слушатель события для кнопки report.

         Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку),
         при нажатии, пытается создать отчет на основе фильмов в таблице, используя JasperReport
        */

        report.addActionListener(e -> {
            if (movies.isEmpty()) {
                JOptionPane.showMessageDialog(frame, "Список фильмов пуст!");
                return;
            }

            Thread thread = new Thread(() -> {
                JProgressBar progressBar = new JProgressBar();
                progressBar.setStringPainted(true);
                progressBar.setString("Создание отчета...");
                progressBar.setIndeterminate(true);
                progressBar.setVisible(true);

                JDialog progressDialog = createDialog(frame, "Создание отчета...", true);
                progressDialog.add(progressBar);
                progressDialog.setVisible(true);

                try {
                    while (Thread.currentThread().isInterrupted())
                        Thread.sleep(100);
                } catch (InterruptedException e1) {
                    progressBar.setVisible(false);
                    progressDialog.setVisible(false);
                }
                progressBar.setVisible(false);
                progressDialog.setVisible(false);
            });

            JFileChooser jfc =
                    new JFileChooser(FileSystemView.getFileSystemView().getParentDirectory(new File("./")));
            jfc.setDialogTitle("Выберите директорию где будет сохранен отчет");
            jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            jfc.setAcceptAllFileFilterUsed(false);

            // Определяем фильтры типов файлов
            for (int i = 0; i < REPORT_FILTERS[0].length; i++) {
                FileFilterExt eff = new FileFilterExt(REPORT_FILTERS[i][0],
                        REPORT_FILTERS[i][1]);
                jfc.addChoosableFileFilter(eff);
            }


            int returnValue = jfc.showSaveDialog(frame);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                try {

//                    thread.start();
                    controller.saveReport(jfc.getSelectedFile());
//                    thread.interrupt();

                    JOptionPane.showMessageDialog(frame, "Отчет был записан на диск");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frame, ex.getMessage());
                }
            }
        });
    }


    /**
     * Функция создания диалогового окна.
     *
     * @param title - заголовок окна
     * @param modal - флаг модальности
     */
    private JDialog createDialog(Frame frame, String title, boolean modal) {
        JDialog dialog = new JDialog(frame, title, modal);
        dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dialog.setSize(200, 90);
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(frame);
        dialog.setLocation(200, 200);
        return dialog;
    }

    private class ProgressDialog {

    }

}

