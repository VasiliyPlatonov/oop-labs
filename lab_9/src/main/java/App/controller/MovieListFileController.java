package App.controller;

import App.Application;
import App.ReportGenerator;
import App.exception.*;
import App.model.Movie;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import App.service.MovieService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the App.controller class for list of movie.<br/>
 * This class works with file system for saving, reading list of movies.
 *
 * @author Vasiliy Platonov
 */
public class MovieListFileController implements MovieListController {

    private List<Movie> movies;

    public MovieListFileController(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     * Save movie list in file
     *
     * @throws EmptyMovieListException if movie list is empty
     */
    @Override
    public File save(File file) throws EmptyMovieListException, UploadMovieException {
        if (movies.isEmpty()) {
            throw new EmptyMovieListException();
        }
        //write to txt file
        if (getFileExtension(file).equals("txt") || getFileExtension(file).equals("")) {
            try (FileWriter writer = new FileWriter(file)) {
                for (Movie movie : movies) {
                    writer.write(movie + System.lineSeparator());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }//write to xml file
        else if (getFileExtension(file).equals("xml")) {

            try {
                DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = dBuilder.newDocument();
//                doc.getDocumentElement().normalize();

                // Создание корневого элемента booklist и добавление его в документ
                Node movielist = doc.createElement("movielist");
                doc.appendChild(movielist);
                // Создание дочерних элементов book и присвоение значений атрибутам
                for (Movie m : movies) {
                    Element movie = doc.createElement("movie");
                    movie.setAttribute("name", m.getName());
                    movie.setAttribute("director", m.getDirector());
                    movie.setAttribute("year", m.getYear());
                    movielist.appendChild(movie);
                }

                // Создание преобразователя документа
                Transformer trans = TransformerFactory.newInstance().newTransformer();
                // Создание файла с именем books.xml для записи документа
                FileWriter fw = new FileWriter(file);
                // Запись документа в файл
                trans.transform(new DOMSource(doc), new StreamResult(fw));

            } catch (TransformerException | IOException | ParserConfigurationException e) {
                throw new UploadMovieException("При работе с XML возникла ошибка", e);
            }
        } else {
            throw new UploadMovieException("Неизвестное расширение файла. Поддерживаются файлы только в формате TXT и XML");
        }
        return file;
    }

    @Override
    public void readFromFile(File f) throws DownloadMoviesException {
        List<Movie> result = new ArrayList<>();

        //read from txt file
        if (getFileExtension(f).equals("txt")) {
            try (BufferedReader br = new BufferedReader(new FileReader(f))) {
                while (br.ready()) {
                    result.add(movieFromString(br.readLine()));
                }
            } catch (IOException | IllegalArgumentException ex) {
                throw new DownloadMoviesException("При чтении списка фильмов из файла " + f.getName() + " произошла ошибка!");
            }
            //read from xml file
        } else if (getFileExtension(f).equals("xml")) {
            try {
                DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = dBuilder.parse(f);
                doc.getDocumentElement().normalize();
                NodeList nlBooks = doc.getElementsByTagName("movie");

                // Цикл просмотра списка элементов и запись данных в таблицу
                for (int temp = 0; temp < nlBooks.getLength(); temp++) {
                    // Выбор очередного элемента списка
                    Node elem = nlBooks.item(temp);
                    // Получение списка атрибутов элемента
                    NamedNodeMap attrs = elem.getAttributes();
                    // Чтение атрибутов элемента
                    String name = attrs.getNamedItem("name").getNodeValue();
                    String director = attrs.getNamedItem("director").getNodeValue();
                    String year = attrs.getNamedItem("year").getNodeValue();
                    // Запись данных в таблицу
//                    App.model.addRow(new String[]{author, title, have});
                    result.add(movieFromString(name + ", " + director + ", " + year));
                }
                // Обработка ошибки парсера при чтении данных из XML-файла
            } catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
            }

        }

        movies.clear();
        movies.addAll(result);
    }

    private static Movie movieFromString(String string) {
        if (string.isEmpty()) {
            throw new IllegalArgumentException("Невозможно прочитать строку - строка пуста");
        }
        String[] movieData = string.split(", ");
        return new Movie(movieData[0], movieData[1], movieData[2]);
    }

    private static String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf + 1);
    }

    public List<Movie> getAllMovie() {
        return movies;
    }


    public void addMovie(Movie movie) throws ValidationException {
        MovieService.checkMovie(movie);
        movies.add(movie);
    }


    public void removeMovie(Movie movie) {
        movies.remove(movie);
    }

    @Override
    public void saveReport(File file) throws Exception {
        try {
            File tmpFile = save(new File(file.getParent() + File.separator + "tmpData.xml"));

            //        String dataSource = "testData.xml";
            String dataSource = tmpFile.getAbsolutePath();
            String template = "report.jrxml";
            ReportGenerator.generate(dataSource, template, file.getAbsolutePath());
            tmpFile.delete();
        } catch (EmptyMovieListException | UploadMovieException e) {
//            e.printStackTrace();
            throw new Exception("При попытке создать отчет, во время создания временного xml файла произошла ошибка.", e);
        }

    }
}

