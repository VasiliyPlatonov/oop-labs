package App.service;

import App.exception.ValidationException;
import App.model.Movie;

public class MovieService {

    public static void checkMovie(Movie movie) throws ValidationException {
        checkMovieName(movie.getName());
        checkDirectorName(movie.getDirector());
        checkYear(movie.getYear());
    }


    public static void checkYear(String value) throws ValidationException {
        int year;

        if (value.isEmpty()) {
            throw new ValidationException("Год выхода фильма не может быть пустым!");
        }

        try {
            year = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new ValidationException("Год выхода фильма должен быть числом и не может содержать букв!");
        }

        if (year < 1800 || year > getCurrentYear() + 20) {
            throw new ValidationException(
                    "Год выхода фильма должен быть больше " + 1800 + " и меньше " + (getCurrentYear() + 20) + " !");
        }
    }


    public static void checkMovieName(String name) throws ValidationException {
        if (name.trim().isEmpty()) {
            throw new ValidationException("Название фильма не может быть пустым!");
        }
    }

    public static void checkDirectorName(String director) throws ValidationException {
        if (director.trim().isEmpty()) {
            throw new ValidationException("Имя режиссера не может быть пустым!");
        }

        if (director.matches(".*\\d.*")) {
            throw new ValidationException("Имя режиссера не может содержать цифры");
        }

    }

    private static int getCurrentYear() {
        java.util.Calendar calendar = java.util.Calendar.getInstance(java.util.TimeZone.getDefault(), java.util.Locale.getDefault());
        calendar.setTime(new java.util.Date());
        return calendar.get(java.util.Calendar.YEAR);
    }
}
