package App.exception;

/**
 * <p>
 * The {@code EmptyMovieListException} is thrown when
 * user tries to save an empty movie list.
 * <p/>
 *
 * @author Vasiliy Platonov
 */
public class EmptyMovieListException extends Exception {
    public EmptyMovieListException() {
        super("Невозможно сохранить список фильмов! Список пуст!");
    }
}
