package App.model;

import App.exception.ValidationException;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * This class implements the ability to App.view movie list as table
 *
 * @author Vasiliy Platonov
 */
public class MovieTableModel extends AbstractTableModel {

    private static final String[] COLUMN_NAMES = {
            "Название",
            "Режиссер",
            "Год"
    };

    private List<Movie> movies;
    private JFrame frame;

    public MovieTableModel(List<Movie> movies, JFrame frame) {
        this.frame = frame;
        this.movies = movies;
    }

    @Override
    public int getRowCount() {
        return movies.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Movie movie = movies.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return movie.getName();
            case 1:
                return movie.getDirector();
            case 2:
                return movie.getYear();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        Movie movie = movies.get(rowIndex);

        switch (columnIndex) {
            case 0:
                movie.setName((String) value);
                break;
            case 1:
                movie.setDirector((String) value);
                break;
            case 2:
                try {
                    checkYear((String) value);
                } catch (ValidationException ex) {
                    JOptionPane.showMessageDialog(frame, ex.getMessage());
                    return;
                }
                movie.setYear((String) value);
                break;
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }


    /*
        Так как isCellEditable всегда
        возвращает true, то абсолютно все ячейки являются
        редактируемыми
        */
    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }


    private static void checkYear(String value) throws ValidationException {
        int year;

        if (value.isEmpty()) {
            throw new ValidationException("Год выхода фильма не может быть пустым!");
        }

        try {
            year = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw new ValidationException("Год выхода фильма должен быть числом и не может содержать букв!");
        }

        if (year < 1800 || year > getCurrentYear() + 20) {
            throw new ValidationException(
                    "Год выхода фильма должен быть больше " + 1800 + " и меньше " + (getCurrentYear() + 20) + " !");
        }
    }

    private static int getCurrentYear() {
        java.util.Calendar calendar = java.util.Calendar.getInstance(java.util.TimeZone.getDefault(), java.util.Locale.getDefault());
        calendar.setTime(new java.util.Date());
        return calendar.get(java.util.Calendar.YEAR);
    }

}
