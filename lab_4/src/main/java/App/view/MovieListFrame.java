package App.view;

import App.controller.MovieListController;
import App.controller.MovieListFileController;
import App.exception.EmptyMovieListException;
import App.model.Movie;
import App.model.MovieTableModel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class MovieListFrame {
    /**
     * Окно для приложения
     */
    private JFrame frame;

    /**
     * Модель таблицы
     */
    private DefaultTableModel model;

    /**
     * Кнопка 'добавить фильм'
     */
    private JButton add;

    /**
     * Кнопка 'удалить фильм'
     */
    private JButton delete;

    /**
     * Кнопка 'редактировать фильм'
     */
    private JButton edit;

    /**
     * Кнопка 'сохранить фильм'
     */
    private JButton save;

    /**
     * Кнопка 'вывести информацию'
     */
    private JButton info;

    /**
     * Панель инстументов
     */
    private JToolBar toolBar;

    /**
     * Скролл
     */
    private JScrollPane scroll;

    /**
     *
     */
    private JTable movieTable;

    /**
     *
     */
    private JComboBox director;

    /**
     *
     */
    private JTextField movieName;

    /**
     *
     */
    private JButton filter;


    private MovieListController mController;
    private List<Movie> movies;

    public MovieListFrame() {
        movies = new ArrayList<>();

        // testdata
        movies.add(new Movie("Оно 2", "Андрес Мусскетти", "2019"));
        movies.add(new Movie("Мстители", "Джосс Уидон", "2012"));
        movies.add(new Movie("Служебный роман", "Эльдар Рязанов", "1977"));
        movies.add(new Movie("Любовь и голуби", "Владимир Меньшов", "1984"));

        mController = new MovieListFileController(movies);
    }

    /**
     * Показывает список фильмов
     */
    public void show() {
        // Создание окна
        frame = new JFrame("Список фильмов");
        frame.setSize(500, 600);
        frame.setLocation(100, 100);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // ничего не делаем, т.к. будет повешен слушатель

        // Создание кнопок и прикрепление иконок
        add = new JButton(new ImageIcon(this.getClass().getResource("../img/add.png")));
        add.setToolTipText("Добавить фильм в список");

        delete = new JButton(new ImageIcon(this.getClass().getResource("../img/remove.png")));
        delete.setToolTipText("Удалить фильм из списка");

        edit = new JButton(new ImageIcon(this.getClass().getResource("../img/notepad.png")));
        edit.setToolTipText("Редактировать фильм");

        save = new JButton(new ImageIcon(this.getClass().getResource("../img/save.png")));
        save.setToolTipText("Сохранить список фильмов");

        info = new JButton(new ImageIcon(this.getClass().getResource("../img/info.png")));
        info.setToolTipText("Информация");


        // Добавление кнопок на панель инструментов
        toolBar = new JToolBar("Панель инструментов");
        toolBar.add(add);
        toolBar.add(delete);
        toolBar.add(edit);
        toolBar.add(save);
        toolBar.add(info);

        // Размещение панели инструментов
        frame.setLayout(new BorderLayout());
        frame.add(toolBar, BorderLayout.NORTH);

        // Создание и размещение таблицы с данными
        movieTable = new JTable(new MovieTableModel(movies, frame));
        scroll = new JScrollPane(movieTable);
        frame.add(scroll, BorderLayout.CENTER);

        // Подготовка компонентов поиска
        director = new JComboBox(
                new String[]{"Режиссер",
                        "Андрес Мусскетти",
                        "Джосс Уидон",
                        "Эльдар Рязанов",
                        "Владимир Меньшов"});
        movieName = new JTextField("Название фильма");
        filter = new JButton("Поиск");
        // Добавление компонентов на панель
        JPanel filterPanel = new JPanel();
        filterPanel.add(director);
        filterPanel.add(movieName);
        filterPanel.add(filter);
        // Размещение панели поиска внизу окна
        frame.add(filterPanel, BorderLayout.SOUTH);
        // Визуализация экранной формы
        frame.setVisible(true);


        // ***************************
        // Слушатели *****************
        //****************************

        /*
         Слушатель события для кнопки info.

         Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
         при появлении этого события, с помощью класса JOptionPane выводит на экран
         диалоговое окно, информирующее пользователя.
         */
        info.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame,
                        "'Список фильмов'" +
                                "\nприложение для создания списков просмотренных фильмов");
            }
        });

        /*
         Слушатель для фрейма bookList.

         Данный слушатель отслеживает появление события windowClosing (нажатие на кнопку закрытия окна)
         и, при появлении этого события, с помощью класса JOptionPane выводит на экран
         диалоговое окно, запрашивающее подтверждение на выход.
        */
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Object[] options = {"Да", "Нет!"};
                int n = JOptionPane
                        .showOptionDialog(e.getWindow(), "Вы уверены, что хотите выйти?",
                                "Подтверждение", JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null, options,
                                options[0]);
                if (n == 0) {
                    e.getWindow().setVisible(false);
                    System.exit(0);
                }
            }
        });

        /*
         Слушатель события для кнопки save.

         Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
         при появлении этого события, пытается записать на диск текущий список фильмов пользователя
        */
        save.addActionListener(e -> {
            try {
                mController.save();
            } catch (EmptyMovieListException ex) {
                JOptionPane.showMessageDialog(frame, ex.getMessage());
            }
        });

    }
}

