package App.controller;

import App.exception.EmptyMovieListException;

public interface MovieListController {
    /**
     * Save list of movie.
     *
     * <p>
     *     There are a lot of possibilities of saving e.g. on a disk, in a cloud or some database.
     *     The class that implements this interface must implement one of the possibilities.
     * <p/>
     */
    void save() throws EmptyMovieListException;
}
