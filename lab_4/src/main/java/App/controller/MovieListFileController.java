package App.controller;

import App.exception.EmptyMovieListException;
import App.model.Movie;

import java.util.List;

/**
 * This is the App.controller class for list of movie.<br/>
 * This class works with file system for saving, reading list of movies.
 *
 * @author Vasiliy Platonov
 */
public class MovieListFileController implements MovieListController {

    private List<Movie> movies;

    public MovieListFileController(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     * Save movie list in file
     *
     * @throws EmptyMovieListException if movie list is empty
     */
    @Override
    public void save() throws EmptyMovieListException {
        if (movies.isEmpty()) {
            throw new EmptyMovieListException();
        }
    }


    public List<Movie> getAllMovie() {
        return null;
    }


    public void addMovie(Movie movie) {
        movies.add(movie);
    }


    public void removeMovie(Movie movie) {
        movies.remove(movie);
    }
}

