import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fonts.SimpleFontFace;
import net.sf.jasperreports.engine.fonts.SimpleFontFamily;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRXmlUtils;
import net.sf.jasperreports.export.*;
import org.w3c.dom.Document;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;


public class ReportGenerator {

    /*
     * http://jasperreports.sourceforge.net/sample.reference/xmldatasource/
     * https://medium.com/@seymorethrottle/jasper-reports-adding-custom-fonts-589b55a52e7c
     *
     *  for avoid warning:
     * --illegal-access=warn --add-opens java.xml/com.sun.org.apache.xerces.internal.util=ALL-UNNAMED
     */
    public static void generate(String dataSource, String template, String resultPath) {
        System.out.println("--------- Создание отчета --------- \n" +
                "Путь к источнику данных(в формате xml):\t" + dataSource + "\n" +
                "Относительный путь к результирующему файлу отчета:\t" + resultPath
        );
        long start = System.currentTimeMillis();
        HashMap<String, Object> params = new HashMap<>();
        params.put("DATE", new Date());


        try {
            // Здесь FutureTask необходим для того чтобы можно было вернуть экземпляр JasperPrint
            FutureTask<JasperPrint> futureTask = new FutureTask<>(new MakeJasperPrint(template, params));

            Thread thread1 = new Thread(new LoadDataFromXml(dataSource, params), "Thread-LoadDataFromXml");
            Thread thread2 = new Thread(futureTask, "Thread-MakeJasperPrint");

            thread1.join();
            thread2.join();

            thread1.start();
            thread2.start();

            // futureTask.get работает так же как и join - блокирует выполнение потока, пока не вернется результат
            JasperPrint print = futureTask.get();
            Thread thread3 = new Thread(new ExporterToHTML(print, resultPath), "Thread-ExporterToHTML");
            thread3.start();
            thread3.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Затрачено времени: " + (System.currentTimeMillis() - start) + " миллисекунд");
    }

    public static void main(String[] args) {
        String dataSource = "testData.xml";
        String template = "report.jrxml";
        String resultPath = "report/result.html";

        generate(dataSource, template, resultPath);
    }

}