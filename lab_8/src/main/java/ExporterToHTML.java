import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import java.util.Objects;

public class ExporterToHTML implements Runnable {
    private JasperPrint print;
    private String resultPath;

    public ExporterToHTML(JasperPrint print, String resultPath) {
        this.print = print;
        this.resultPath = resultPath;
    }

    @Override
    public void run() {
        System.out.println("Run ExporterToHTML");
        try {
            String absPath = this.getClass().getResource(resultPath).getPath();
            JasperExportManager.exportReportToHtmlFile(print, absPath);
            System.out.println("Отчет создан по адресу: "+ absPath);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }
}
