import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRXmlUtils;
import org.w3c.dom.Document;

import java.util.HashMap;

public class LoadDataFromXml implements Runnable {
    private String dataSource;
    private HashMap<String, Object> params;

    public LoadDataFromXml(String dataSource, HashMap<String, Object> params) {
        this.dataSource = dataSource;
        this.params = params;
    }

    @Override
    public void run() {
        System.out.println("Run LoadDataFromXml");
        Document document = null;
        try {
            document = JRXmlUtils.parse(JRLoader.getLocationInputStream(dataSource));
        } catch (JRException e) {
            e.printStackTrace();
        }
        params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, document);
    }
}
