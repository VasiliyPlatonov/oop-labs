import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;

import java.util.HashMap;
import java.util.concurrent.*;

public class MakeJasperPrint implements Callable<JasperPrint> {
    private String template;
    private HashMap<String, Object> params;

    public MakeJasperPrint(String template, HashMap<String, Object> params) {
        this.template = template;
        this.params = params;
    }

    @Override
    public JasperPrint call() throws JRException {
        System.out.println("Run MakeJasperPrint");
        JasperReport jasperReport = JasperCompileManager.compileReport(JRLoader.getLocationInputStream(template));
        return JasperFillManager.fillReport(jasperReport, params);

    }
}
