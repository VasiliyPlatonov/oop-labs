import App.view.MovieListFrame;

/**
 * Экранная форма предназначена для отображения списка фильмов,
 * она может менять свой размер на экране. Форма реализовывает следующие
 * функции: загрузку списка фильмов из файла, сохранение списка фильмов в файле,
 * добавление фильма в список, редактирование данных о фильме
 * удаление фильма из списка, поиск фильма по названию, режиссеру
 * вывод информации о приложении.
 *
 * @author Vasiliy Platonov
 * @version 1.0
 */
public class Application {
    private MovieListFrame frame;

    public Application() {
        frame = new MovieListFrame();
    }

    public static void main(String[] args) {
        // Создание и отображение приложения
        new Application().show();
    }

    private void show() {
        frame.show();
    }

}
