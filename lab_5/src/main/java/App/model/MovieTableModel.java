package App.model;

import App.exception.ValidationException;
import App.service.MovieService;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * This class implements the ability to App.view movie list as table
 *
 * @author Vasiliy Platonov
 */

public class MovieTableModel extends AbstractTableModel {

    private static final String[] COLUMN_NAMES = {
            "Название",
            "Режиссер",
            "Год"
    };

    private List<Movie> movies;
    private JFrame frame;

    public MovieTableModel(List<Movie> movies, JFrame frame) {
        this.frame = frame;
        this.movies = movies;
    }

    @Override
    public int getRowCount() {
        return movies.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }



    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Movie movie = movies.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return movie.getName();
            case 1:
                return movie.getDirector();
            case 2:
                return movie.getYear();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        Movie movie = movies.get(rowIndex);
        try {
            MovieService.checkYear((String) value);
            switch (columnIndex) {
                case 0:
                    MovieService.checkMovieName((String) value);
                    movie.setName((String) value);
                    break;
                case 1:
                    MovieService.checkDirectorName((String) value);
                    movie.setDirector((String) value);
                    break;
                case 2:
                    MovieService.checkYear((String) value);
                    movie.setYear((String) value);
                    break;
            }
        } catch (ValidationException ex) {
            JOptionPane.showMessageDialog(frame, ex.getMessage());
            return;
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }


    /*
        Так как isCellEditable всегда
        возвращает true, то абсолютно все ячейки являются
        редактируемыми
        */
    @Override
    public boolean isCellEditable(int row, int column) {
        return true;
    }


}
