package App.view;

import App.controller.MovieListController;
import App.controller.MovieListFileController;
import App.exception.DownloadMoviesException;
import App.exception.EmptyMovieListException;
import App.exception.ValidationException;
import App.model.Movie;
import App.model.MovieTableModel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MovieListFrame {
    /**
     * Окно для приложения
     */
    private JFrame frame;

    /**
     * Модель таблицы
     */
    private DefaultTableModel model;

    /**
     * Кнопка 'добавить фильм'
     */
    private JButton add;

    /**
     * Кнопка 'удалить фильм'
     */
    private JButton delete;

    /**
     * Кнопка 'редактировать фильм'
     */
    private JButton edit;

    /**
     * Кнопка 'сохранить список фильмов в файл'
     */
    private JButton upload;

    /**
     * Кнопка 'загрузить фильмовов из файл'
     */
    private JButton download;


    /**
     * Кнопка 'вывести информацию'
     */
    private JButton info;

    /**
     * Панель инстументов
     */
    private JToolBar toolBar;

    /**
     * Скролл
     */
    private JScrollPane scroll;

    /**
     *
     */
    private JTable movieTable;

    /**
     *
     */
    private JComboBox director;

    /**
     *
     */
    private JTextField movieName;

    /**
     *
     */
    private JButton filter;


    private MovieListController controller;
    private List<Movie> movies;

    public MovieListFrame() {
        movies = new ArrayList<>();

        // testdata
//        movies.add(new Movie("Оно 2", "Андрес Мусскетти", "2019"));
//        movies.add(new Movie("Мстители", "Джосс Уидон", "2012"));
//        movies.add(new Movie("Служебный роман", "Эльдар Рязанов", "1977"));
//        movies.add(new Movie("Любовь и голуби", "Владимир Меньшов", "1984"));

        controller = new MovieListFileController(movies);
    }

    /**
     * Показывает список фильмов
     */
    public void show() {
        // Создание окна
        frame = new JFrame("Список фильмов");
        frame.setSize(500, 600);
        frame.setLocation(100, 100);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // ничего не делаем, т.к. будет повешен слушатель

        // Создание кнопок и прикрепление иконок
        ImageIcon addIcon = new ImageIcon(this.getClass().getResource("../img/add.png"));
        add = new JButton(addIcon);
        add.setToolTipText("Добавить фильм в список");

        ImageIcon deleteIcon = new ImageIcon(this.getClass().getResource("../img/remove.png"));
        delete = new JButton(deleteIcon);
        delete.setToolTipText("Удалить фильм из списка");

        ImageIcon editIcon = new ImageIcon(this.getClass().getResource("../img/notepad.png"));
        edit = new JButton(editIcon);
        edit.setToolTipText("Редактировать фильм");

        ImageIcon uploadIcon = new ImageIcon(this.getClass().getResource("../img/upload.png"));
        upload = new JButton(uploadIcon);
        upload.setToolTipText("Сохранить список фильмов в файл");

        ImageIcon downloadIcon = new ImageIcon(this.getClass().getResource("../img/download.png"));
        download = new JButton(downloadIcon);
        download.setToolTipText("Загрузить список фильмов из файла");


        ImageIcon infoIcon = new ImageIcon(this.getClass().getResource("../img/info.png"));
        info = new JButton(infoIcon);
        info.setToolTipText("Информация");


        // Добавление кнопок на панель инструментов
        toolBar = new JToolBar("Панель инструментов");
        toolBar.add(add);
        toolBar.add(delete);
        toolBar.add(edit);
        toolBar.add(upload);
        toolBar.add(download);
        toolBar.add(info);

        // Размещение панели инструментов
        frame.setLayout(new BorderLayout());
        frame.add(toolBar, BorderLayout.NORTH);

        // Создание и размещение таблицы с данными
        MovieTableModel tableModel = new MovieTableModel(movies, frame);
        movieTable = new JTable(tableModel);
        scroll = new JScrollPane(movieTable);
        frame.add(scroll, BorderLayout.CENTER);

        // Подготовка компонентов поиска
        director = new JComboBox(
                new String[]{"Режиссер",
                        "Андрес Мусскетти",
                        "Джосс Уидон",
                        "Эльдар Рязанов",
                        "Владимир Меньшов"});
        movieName = new JTextField("Название фильма");
        filter = new JButton("Поиск");
        // Добавление компонентов на панель
        JPanel filterPanel = new JPanel();
        filterPanel.add(director);
        filterPanel.add(movieName);
        filterPanel.add(filter);
        // Размещение панели поиска внизу окна
        frame.add(filterPanel, BorderLayout.SOUTH);
        // Визуализация экранной формы
        frame.setVisible(true);


        // ***************************
        // Слушатели *****************
        //****************************

        /*
         Слушатель события для кнопки info.

         Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
         при появлении этого события, с помощью класса JOptionPane выводит на экран
         диалоговое окно, информирующее пользователя.
         */
        info.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame,
                        "'Список фильмов'" +
                                "\nприложение для создания списков просмотренных фильмов");
            }
        });

        /*
         Слушатель для фрейма bookList.

         Данный слушатель отслеживает появление события windowClosing (нажатие на кнопку закрытия окна)
         и, при появлении этого события, с помощью класса JOptionPane выводит на экран
         диалоговое окно, запрашивающее подтверждение на выход.
        */
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Object[] options = {"Да", "Нет!"};
                int n = JOptionPane
                        .showOptionDialog(e.getWindow(), "Вы уверены, что хотите выйти?",
                                "Подтверждение", JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null, options,
                                options[0]);
                if (n == 0) {
                    e.getWindow().setVisible(false);
                    System.exit(0);
                }
            }
        });

        /*
         Слушатель события для кнопки upload.

         Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
         при появлении этого события, пытается записать на диск текущий список фильмов пользователя
        */
        upload.addActionListener(e -> {
            try {
                controller.save();
            } catch (EmptyMovieListException ex) {
                JOptionPane.showMessageDialog(frame, ex.getMessage());
            }
            JOptionPane.showMessageDialog(frame, "Список фильмов был записан на диск");
        });

       /*
        Слушатель события для кнопки download.

        Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
        при появлении этого события, пытается загузить список фильмов из файла
        */
        download.addActionListener(e -> {
            String path = JOptionPane.showInputDialog(frame, "Введите путь к файлу");

            try {
                controller.readFromFile(new File(path));
                tableModel.fireTableDataChanged();
            } catch (DownloadMoviesException ex) {
                JOptionPane.showMessageDialog(frame, ex.getMessage());
            }
        });


        /*
        Слушатель события для кнопки add.

        Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
        при появлении этого события, пытается добавить фильм в список фильмов
        */
        add.addActionListener(e -> {
            JPanel myPanel = new JPanel();
            JTextField name = new JTextField(10);
            JTextField director = new JTextField(10);
            JTextField year = new JTextField(10);
            myPanel.add(new JLabel("Название: "));
            myPanel.add(name);
            myPanel.add(new JLabel("Режиссер: "));
            myPanel.add(director);
            myPanel.add(new JLabel("Год: "));
            myPanel.add(year);

            int exit =
                    JOptionPane.showConfirmDialog(frame, myPanel,
                            "Введите данные фильма",
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            addIcon);

            if (exit == JOptionPane.YES_OPTION) {
                try {
                    controller.addMovie(
                            new Movie(
                                    name.getText(),
                                    director.getText(),
                                    year.getText()));
                } catch (ValidationException ex) {
                    JOptionPane.showMessageDialog(frame, ex.getMessage());
                }
                tableModel.fireTableDataChanged();
            }
        });

         /*
        Слушатель события для кнопки delete.

        Данный слушатель отслеживает появление события ActionEvent (нажатие на кнопку) и,
        при появлении этого события, пытается удалить фильм выбранный из списка фильмов
        */

        delete.addActionListener(e -> {
            if (movies.isEmpty()) {
                JOptionPane.showMessageDialog(frame, "Список фильмов пуст!");
                return;
            }
            if (movieTable.getSelectedRowCount() > 1) {
                int[] selectedRows = movieTable.getSelectedRows();

                for (int i = selectedRows.length - 1; i >= 0; i--) {
                    controller.removeMovie(movies.get(selectedRows[i]));
                }

            } else {
                controller.removeMovie(movies.get(movieTable.getSelectedRow()));
            }

            tableModel.fireTableDataChanged();
        });
    }
}

