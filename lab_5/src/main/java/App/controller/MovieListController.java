package App.controller;

import App.exception.DownloadMoviesException;
import App.exception.EmptyMovieListException;
import App.exception.ValidationException;
import App.model.Movie;

import java.io.File;

public interface MovieListController {
    /**
     * Save list of movie.
     *
     * <p>
     * There are a lot of possibilities of saving e.g. on a disk, in a cloud or some database.
     * The class that implements this interface must implement one of the possibilities.
     * <p/>
     */
    void save() throws EmptyMovieListException;


    /**
     * Read list of movie from a file.
     */
    void readFromFile(File f) throws DownloadMoviesException;

    /**
     * Add movie to movie list.
     */
    void addMovie(Movie movie) throws ValidationException;

    /**
     * Remove movie from movie list
     */

    void removeMovie(Movie movie);
}
