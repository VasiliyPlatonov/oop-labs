package App.controller;

import App.exception.DownloadMoviesException;
import App.exception.EmptyMovieListException;
import App.exception.ValidationException;
import App.model.Movie;
import App.service.MovieService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the App.controller class for list of movie.<br/>
 * This class works with file system for saving, reading list of movies.
 *
 * @author Vasiliy Platonov
 */
public class MovieListFileController implements MovieListController {

    private List<Movie> movies;

    public MovieListFileController(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     * Save movie list in file
     *
     * @throws EmptyMovieListException if movie list is empty
     */
    @Override
    public void save() throws EmptyMovieListException {
        if (movies.isEmpty()) {
            throw new EmptyMovieListException();
        }

        try (FileWriter writer = new FileWriter("output.txt")) {
            for (Movie movie : movies) {
                writer.write(movie + System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void readFromFile(File f) throws DownloadMoviesException {
        List<Movie> result = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            while (br.ready()) {
                result.add(movieFromString(br.readLine()));
            }
        } catch (IOException | IllegalArgumentException ex) {
            throw new DownloadMoviesException("При чтении списка фильмов из файла " + f.getName() + " произошла ошибка!");
        }

        movies.clear();
        movies.addAll(result);
    }

    private static Movie movieFromString(String string) {
        if (string.isEmpty()) {
            throw new IllegalArgumentException("Невозможно прочитать строку - строка пуста");
        }
        String[] movieData = string.split(", ");
        return new Movie(movieData[0], movieData[1], movieData[2]);
    }

    public List<Movie> getAllMovie() {
        return null;
    }


    public void addMovie(Movie movie) throws ValidationException {
        MovieService.checkMovie(movie);
        movies.add(movie);
    }


    public void removeMovie(Movie movie) {
        movies.remove(movie);
    }
}

